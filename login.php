<?php
session_start();
if(@$_SESSION['username']!=''){
	header('location: dashboard.php');
}
?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Login form using HTML5 and CSS3</title>
    
    
    
    
        <link rel="stylesheet" href="css/style.css">

    
    
    
  </head>

  <body>
<div class="container">
	<section id="content">
		<form action="function/function_login.php" method="POST">
			<h1>Login Form</h1>
			<div>
				<input type="text" placeholder="Username" name="username" required="" id="username" />
			</div>
			<div>
				<input type="password" placeholder="Password" name="password" required="" id="password" />
			</div>
			<div>
				<input type="hidden" name="tipe" value="1">
				<input type="submit" value="Log in" />
				<a href="#">Lost your password?</a>
				<a href="#">Register</a>
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="index.php">Back</a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
    
        <script src="js/index.js"></script>
</html>
