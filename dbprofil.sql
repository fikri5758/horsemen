-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.13-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_profil
CREATE DATABASE IF NOT EXISTS `db_profil` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_profil`;


-- Dumping structure for table db_profil.siswa
CREATE TABLE IF NOT EXISTS `siswa` (
  `siswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_nis` int(20) NOT NULL,
  `siswa_nama` varchar(50) NOT NULL,
  `siswa_kelas` varchar(10) NOT NULL,
  `siswa_jurusan` varchar(30) NOT NULL,
  PRIMARY KEY (`siswa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table db_profil.t_berita
CREATE TABLE IF NOT EXISTS `t_berita` (
  `id_berita` int(5) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(60) DEFAULT NULL,
  `isi_berita` text,
  `tanggal_berita` date DEFAULT NULL,
  `id_kategori` int(5) DEFAULT NULL,
  `id_user` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_profil.t_header
CREATE TABLE IF NOT EXISTS `t_header` (
  `id_header` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) DEFAULT '0',
  `isi` varchar(200) DEFAULT '0',
  PRIMARY KEY (`id_header`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_profil.t_identitas
CREATE TABLE IF NOT EXISTS `t_identitas` (
  `id_identitas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_web` varchar(100) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `meta_deskripsi` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  `email_identitas` varchar(100) DEFAULT NULL,
  `no_hp` varchar(13) DEFAULT NULL,
  `favicon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_identitas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_profil.t_kategori
CREATE TABLE IF NOT EXISTS `t_kategori` (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_profil.t_posting
CREATE TABLE IF NOT EXISTS `t_posting` (
  `id_posting` int(11) NOT NULL AUTO_INCREMENT,
  `judul_posting` varchar(100) NOT NULL DEFAULT '0',
  `isi_posting` text,
  `gambar_posting` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_posting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_profil.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
